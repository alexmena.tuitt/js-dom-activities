let nav = document.querySelector('nav')

let navCont = `
	<ul class="flex">
		<li class="m-3">
			<a href="index.html">Level 1</a>
		</li>
		<li class="m-3">
			<a href="level-2.html">Level 2</a>
		</li>
		<li class="m-3">
			<a href="level-3.html">Level 3</a>
		</li>
		<li class="m-3">
			<a href="level-4.html">Level 4</a>
		</li>
		<li class="m-3">
			<a href="level-5.html">Final</a>
		</li>
	</ul>
`

nav.innerHTML = navCont

// content

let wrapperLvl = parseInt(document.querySelector('#wrapper').dataset.level)


let addTeam1ScoreBtn = document.querySelector('#addTeam1ScoreBtn')
let team1Span = document.querySelector('#team1Span')
addTeam1ScoreBtn.addEventListener('click', () => {
	team1Span.innerHTML = parseInt(team1Span.innerHTML) + 1
})

if (wrapperLvl >= 2){

	let addTeam2ScoreBtn = document.querySelector('#addTeam2ScoreBtn')
	let team2Span = document.querySelector('#team2Span')
	addTeam2ScoreBtn.addEventListener('click', () => {
		team2Span.innerHTML = parseInt(team2Span.innerHTML) + 1
	})
}


if (wrapperLvl >= 3){
	let team1NameInp = document.querySelector('#team1NameInp')
	let team1Name = document.querySelector('#team1Name')
	team1NameInp.addEventListener('input', (e) => {
		team1Name.innerHTML = e.target.value
		if (wrapperLvl >= 5){
			document.querySelector('#team1Scores').innerHTML = e.target.value
		}
	})
}

if (wrapperLvl >= 4){
	let team2NameInp = document.querySelector('#team2NameInp')
	let team2Name = document.querySelector('#team2Name')
	team2NameInp.addEventListener('input', (e) => {
		team2Name.innerHTML = e.target.value
		if (wrapperLvl >= 5){
			document.querySelector('#team2Scores').innerHTML = e.target.value
		}
	})
}

if (wrapperLvl >= 5){
	let roundNumberInput = document.querySelector('#roundNumberInput')
	let recordScore = document.querySelector('#recordScore')

	// tally scores
	let game = {
		finalScore : {
			team1 : 0,
			team2 : 0
		},
		tallies: [
		]
	}


	function tally(){
		game.tallies = [
			...game.tallies,
			{
				round : roundNumberInput.value,
				team1 : parseInt(team1Span.innerHTML),
				team2 : parseInt(team2Span.innerHTML),
			}
		]
	}

	function computeFinal(){
		team1FinalScore = 0
		team2FinalScore = 0
		game.tallies.forEach( tally => {
			team1FinalScore += tally.team1
			team2FinalScore += tally.team2
		})
		game.finalScore ={
			team1 : team1FinalScore,
			team2 : team2FinalScore
		}
	}

	function clearScores() {
		roundNumberInput.value++
		team1Span.innerHTML = 0
		team2Span.innerHTML = 0
	}

	function displayTallies(){
		let tbody = document.querySelector('tbody')


		let tbodyCont = game.tallies.map( tally => {
			return `<tr>
				<th>${tally.round}</th>
				<td>${tally.team1}</td>
				<td>${tally.team2}</td>
			</tr>`
		}).join("")

		tbody.innerHTML = tbodyCont
	}

	function displayFinal(){
		let tfoot = document.querySelector('tfoot')
		tfoot.innerHTML = `
			<tr>
				<th class="p-3">Final Score </th>
				<th>${game.finalScore.team1}</th>
				<th>${game.finalScore.team2}</th>
			</tr>
		`
	}
	// record score
	
	recordScore.addEventListener('submit',(e)=>{
		e.preventDefault()

		tally()
		computeFinal()
		clearScores()
		displayTallies()
		displayFinal()

	})


}